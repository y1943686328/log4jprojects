package com.example.androidlog4j2project;

import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.spi.ExtendedLogger;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    private static final Marker NAME = MarkerManager.getMarker("NAME");
    private static final Marker ERVIN = MarkerManager.getMarker("ERVIN");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        ExtendedLogger logger=null;
        InputStream open = null;
        try {
            open = getAssets().open("log4j2.xml");
            ConfigurationSource source = new ConfigurationSource(open);
            LoggerContext initialize = Configurator.initialize(null, source);
            logger =  initialize.getLogger(MainActivity.class);
        } catch (IOException e) {
            e.printStackTrace();
            try {
                open.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }

        logger.error("error");
        logger.debug("debug");
        logger.fatal("fatal");
        logger.warn("warn");
        logger.info("info");
        logger.trace("trace");

        logger.log(Level.DEBUG, NAME, "marker debug");
        logger.log(Level.ERROR, ERVIN, "marker error");
        logger.log(Level.FATAL, NAME, "marker fatal");
        logger.log(Level.WARN, ERVIN, "marker warn");
        logger.log(Level.INFO, NAME, "marker info");
        logger.log(Level.TRACE, NAME, "marker trace");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}